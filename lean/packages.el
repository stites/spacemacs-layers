;;; packages.el --- lean layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2017 Sylvain Benner & Contributors
;;
;; Author: Sam <stites@lambek>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;;; Commentary:

;; See the Spacemacs documentation and FAQs for instructions on how to implement
;; a new layer:
;;
;;   SPC h SPC layers RET
;;
;;
;; Briefly, each package to be installed or configured by this layer should be
;; added to `lean-packages'. Then, for each package PACKAGE:
;;
;; - If PACKAGE is not referenced by any other Spacemacs layer, define a
;;   function `lean/init-PACKAGE' to load and initialize the package.

;; - Otherwise, PACKAGE is already referenced by another Spacemacs layer, so
;;   define the functions `lean/pre-init-PACKAGE' and/or
;;   `lean/post-init-PACKAGE' to customize the package as it is loaded.

;;; Code:

(defconst lean-packages
  '(
    (lean-mode     :location elpa)
    (company-lean  :location elpa)
    (helm-lean     :location elpa)
  )

  "The list of Lisp packages required by the lean layer.

Each entry is either:

1. A symbol, which is interpreted as a package to be installed, or

2. A list of the form (PACKAGE KEYS...), where PACKAGE is the
    name of the package to be installed or loaded, and KEYS are
    any number of keyword-value-pairs.

    The following keys are accepted:

    - :excluded (t or nil): Prevent the package from being loaded
      if value is non-nil

    - :location: Specify a custom installation location.
      The following values are legal:

      - The symbol `elpa' (default) means PACKAGE will be
        installed using the Emacs package manager.

      - The symbol `local' directs Spacemacs to load the file at
        `./local/PACKAGE/PACKAGE.el'

      - A list beginning with the symbol `recipe' is a melpa
        recipe.  See: https://github.com/milkypostman/melpa#recipe-format")

(defun lean/init-lean-mode ()
  (use-package lean-mode :defer t))
(defun lean/init-company-lean ()
  (use-package company-lean :defer t))
(defun lean/init-helm-lean ()
  (use-package helm-lean :defer t))


; M-. lean-find-definition
; M-, xref-pop-marker-stack ; jump back to position before M-.
; C-c C-k ; show keystroke needed to input the symbol under the cursor (I assume unicode)
; C-c C-x ; lean-std-exe ; execute lean in standalonnne mode
; C-c SPC lean-hole ; run a command on the hole at that point
; C-c C-d helm-lean-definitions ; searchable list of defs
; C-c C-g lean-toggle-show-goal
; C-c C-n lean-toggle-next-error
; C-c C-b lean-message-boxes-toggle
; C-c C-r lean-server-restart
; C-c C-s lean-server-switch-version
; C-c ! n ; flycheck go to next error
; C-c ! p ; flycheck go to previous error
; C-c ! l ; flycheck show list of errors


;;; packages.el ends here
