;;; packages.el --- org-roam layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2018 Sylvain Benner & Contributors
;;
;; Author: Sam <stites@lambek>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;;; Commentary:

;; See the Spacemacs documentation and FAQs for instructions on how to implement
;; a new layer:
;;
;;   SPC h SPC layers RET
;;
;;
;; Briefly, each package to be installed or configured by this layer should be
;; added to `org-roam-packages'. Then, for each package PACKAGE:
;;
;; - If PACKAGE is not referenced by any other Spacemacs layer, define a
;;   function `org-roam/init-PACKAGE' to load and initialize the package.

;; - Otherwise, PACKAGE is already referenced by another Spacemacs layer, so
;;   define the functions `org-roam/pre-init-PACKAGE' and/or
;;   `org-roam/post-init-PACKAGE' to customize the package as it is loaded.

;;; Code:
;; ==================================================== ;;
;; See https://org-roam.readthedocs.io/en/develop/installation
;; ==================================================== ;;

(defconst org-roam-packages
  '(
    (org-roam
     :location
     (recipe :fetcher github :repo "jethrokuan/org-roam" :branch "develop"))
    (el-patch
     :location
     (recipe :fetcher github :repo "raxod502/el-patch" :branch "develop"))))



(defun org-roam/init-org-roam ()
  (use-package org-roam
    :hook
    (after-init . org-roam-mode)
    :custom
    (org-roam-directory "/path/to/org-files/")
    :init
    (progn
      (spacemacs/declare-prefix "aR" "org-roam")
      (spacemacs/set-leader-keys
        "aRl" 'org-roam
        "aRt" 'org-roam-today
        "aRf" 'org-roam-find-file
        "aRg" 'org-roam-show-graph)

      (spacemacs/declare-prefix-for-mode 'org-mode "mr" "org-roam")
      (spacemacs/set-leader-keys-for-major-mode 'org-mode
        "rl" 'org-roam
        "rt" 'org-roam-today
        "rb" 'org-roam-switch-to-buffer
        "rf" 'org-roam-find-file
        "ri" 'org-roam-insert
        "rg" 'org-roam-show-graph))))

(defun org-roam/init-el-patch ()
  (eval-when-compile
     (require 'el-patch)))

(defun org-roam/init-deft ()
  :config/el-path
  (defun deft-parse-title (file contents)
    "Parse the given FILE and CONTENTS and determine the title.
    If `deft-use-filename-as-title' is nil, the title is taken to
    be the first non-empty line of the FILE. Else the base name of the
    FILE is used as title."
    (el-patch-swap
     (if deft-use-filename-as-title
      (deft-base-filename file
       (let ((begin (string-match "^.+$" contents)))
        (if begin
          (funcall deft-parse-title-function)
          (substring contents begin (match-end 0))

          (org-roam--get-title-or-slug file))))))))


;;; packages.el ends here
